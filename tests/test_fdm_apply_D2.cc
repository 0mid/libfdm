// test_fdm_apply_D2.cc ---

// Copyright (C) 2016 Omid Khanmohamadi <>

// Author: Omid Khanmohamadi <>

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "src/finite_diff_method.h"
#include "tst_functions.h"

#include "gtest/gtest.h"	// for Google Test framework

#include <stdlib.h>		// for malloc(), free(), NULL

// Use the TEST macro to define your tests.
//
// TEST has two parameters: the test case name and the test name.
// After using the macro, you should define your test logic between a
// pair of braces.  You can use a bunch of macros to indicate the
// success or failure of a test.  EXPECT_TRUE and EXPECT_EQ are
// examples of such macros.  For a complete list, see gtest.h.
//
// The test case name and the test name should both be valid C++
// identifiers.  And you should not use underscore (_) in the names.
//
// Google Test guarantees that each test you define is run exactly
// once, but it makes no guarantee on the order the tests are
// executed.  Therefore, you should write your tests in such a way
// that their results don't depend on their order.

// This file must be linked against libgtest_main.a (compiled from
// ${GTEST_DIR}/src/gtest_main.cc), which consists of a main()
// function which calls RUN_ALL_TESTS(), in turn running all the tests
// defined above. It returns 0 if successful, or 1 otherwise.
//
// Notice that we don't register the tests. The RUN_ALL_TESTS() macro
// 'magically' knows about all the tests we defined.

TEST(fdm_apply_test, D2F_from_div_diff_DF_from_FDMapply_F_qform1d)
{
  vec_function F;
  F.function = qform1d_vec_fn;
  F.xdim = 1;
  F.fdim = 1;

  double a = 0.5;
  F.params = &a;

  double qform1d_value;
  F.value = &qform1d_value;

  unsigned nnodes = 2;
  fdm method;
  fdm_type method_type = fwd;
  unsigned deriv_order = 1;
  fdm_alloc(&method, nnodes, F.xdim);
  fdm_set_type(&method, deriv_order, method_type);
  fdm_set_weights(&method);

  double x = 1.0;
  double v = 1.0;
  fdm_calc_nodes(&method, &x, &v); // x + jhv, 0≤j<n
  double DFx_v[2];
  fdm_apply(&method, &F, &DFx_v[0]); // ≈ DF(x)(v)

  double u = 3.0;	      // new arg. for bilinear form D2F(x)(u,v)
  double s = 1e-6;
  x += s*u;
  fdm_calc_nodes(&method, &x, &v);   // (x+su) + jhv, 0≤j<n
  fdm_apply(&method, &F, &DFx_v[1]); // ≈ DF(x + su)(v)

  // The second derivative of a function F: U ⊂ X → Y at a point x ∈
  // U, denoted D²F(x), (if it exists) is a bilinear mapping, an
  // element of L(X, L(X,Y)), naturally identified with L(X,X; Y), the
  // space of bounded bilinear mappings of X×X into Y.
  //
  // ≈ D²F(x)(u,v) = lim s → 0 ( DF(x+su)(v) - DF(x)(v) )/s.
  double D2Fx_uv = ( DFx_v[1] - DFx_v[0] )/s;

  double relerr = 1e-4;
  double D2Fx_wv_expected = 2*a*u*v;
  double abserr = relerr * D2Fx_wv_expected;

  EXPECT_NEAR(D2Fx_uv, D2Fx_wv_expected, abserr);

  fdm_free(&method);
}

TEST(fdm_apply_test,
     D2Fx_vv_Taylor_FDMapply_D1_cen_4node_DFx_v_from_F_qform1d)
{
  vec_function F;
  F.function = qform1d_vec_fn;
  F.xdim = 1;
  F.fdim = 1;

  double a = 0.5;
  F.params = &a;

  double qform1d_value;
  F.value = &qform1d_value;

  unsigned	nnodes	    = 4;
  fdm		method;
  fdm_type	method_type = cen;
  unsigned	deriv_order = 1;

  fdm_alloc(&method, nnodes, F.xdim);
  fdm_set_type(&method, deriv_order, method_type);
  fdm_set_weights(&method);

  double x = 2.0;
  double v = 3.0;
  fdm_calc_nodes(&method, &x, &v);
  double DFx_v;
  fdm_apply(&method, &F, &DFx_v);

  double D2Fx_vv = 0.0;
  double x_plus_v = x + v;

  VEC_FN_EVAL(&F, &x_plus_v);
  D2Fx_vv += F.value[0];	// add F(x+v)

  VEC_FN_EVAL(&F, &x);
  D2Fx_vv -= F.value[0] + DFx_v; // subtract F(x) and DF(x)(v)

  D2Fx_vv *= 2.0;		// scale by 2!

  double	relerr		 = 1e-5;
  double	D2Fx_vv_expected = 2*a*v*v; // D²F(x)(v,v)
  double	abserr		 = relerr * D2Fx_vv_expected;

  EXPECT_NEAR(D2Fx_vv, D2Fx_vv_expected, abserr);

  fdm_free(&method);
}

TEST(fdm_apply_test, D2F_from_FDMapply_DF_from_FDMapply_F_qform1d)
{
  typedef struct vec_fn_params_s {
    vec_function *function;
    fdm *method;
    double *v;
  } vec_fn_params;

  // For any fixed v ∈ X (which is sent in as a parameter), the
  // following defines a finite difference approximation to DF(.)(v):
  // U ⊂ X → Y, x → DF(x)(v). This is neither DF(x)(.): X → Y (x ∈ U,
  // fixed) nor DF(.): U ⊂ X → L(X,Y).
  auto DF_v_vec_fn = [](unsigned xdim, const double * const x, void *params,
			unsigned fdim, double * const value)
    {
      vec_fn_params *p = (vec_fn_params *) params;

      // Calculate the nodes {x+jhv}, 0≤j<D1_nnodes.
      fdm_calc_nodes(p->method, x, p->v);

      // Compute ∑w_jF(x+jhv)/h ≈ lim h → 0 (F(x+hv) - F(x))/h = DF(x)(v).
      fdm_apply(p->method, p->function, value);
    };

  // F(.): U ⊂ X → Y
  vec_function F;
  F.function = qform1d_vec_fn;
  F.xdim = 1;
  F.fdim = 1;

  double a = 0.5;
  F.params = &a;

  double Fx;
  F.value = &Fx;

  unsigned D1_nnodes = 2;
  fdm D1_method;
  fdm_type D1_method_type = fwd;
  unsigned deriv_order = 1;
  fdm_alloc(&D1_method, D1_nnodes, F.xdim);
  fdm_set_type(&D1_method, deriv_order, D1_method_type);
  fdm_set_weights(&D1_method);

  // DF(.)(v): U ⊂ X → Y
  vec_function DF_v;
  DF_v.function = DF_v_vec_fn;

  // These dimensions are equal, since, for any fixed v ∈ X, both F(.)
  // and DF(.)(v) are functions from X to Y.
  DF_v.xdim = F.xdim;
  DF_v.fdim = F.fdim;

  double DFx_v;
  DF_v.value = &DFx_v;

  fdm D2_method;
  unsigned D2_nnodes = 2;

  fdm_type D2_method_type = fwd;

  // We use a fwd finite difference method above to define an
  // approximation to the first derivative (D1), with deriv_order ==
  // 1, which is then used here to approximate the second derivative
  // (D2), also with deriv_order == 1.
  fdm_alloc(&D2_method, D2_nnodes, DF_v.xdim);
  fdm_set_type(&D2_method, deriv_order, D2_method_type);
  fdm_set_weights(&D2_method);

  double x = 1.0;
  double v = 1.0;

  vec_fn_params DF_v_params;
  DF_v.params = &DF_v_params;
  DF_v_params.function = &F;
  DF_v_params.method = &D1_method;
  DF_v_params.v = &v;


  // Given u, calculate the nodes {x_j}, 0≤j<D2_nnodes.
  double u = 3.0;
  fdm_calc_nodes(&D2_method, &x, &u);

  // D²F(x)(.,.) is a bounded bilinear mapping from X×X to Y. Since
  // here we have Y = Rᶠᵈⁱᵐ = R¹, one double is precisely the storage
  // needed for D2Fx_uv.
  double D2Fx_uv;

  fdm_set_type(&D2_method, deriv_order, D2_method_type);
  fdm_set_weights(&D2_method);

  // corresponding to D2_method_type, compute the finite difference
  // approximation to D²F(x)(u,v) using ∑w_jDF(x_j)(v)/h, where v ∈ X
  // is fixed.
  fdm_apply(&D2_method, &DF_v, &D2Fx_uv);

  double relerr = 1e-4;
  double D2Fx_uv_expected = 2*a*u*v;
  double abserr = relerr * D2Fx_uv_expected;

  EXPECT_NEAR(D2Fx_uv, D2Fx_uv_expected, abserr);

  fdm_free(&D1_method);
  fdm_free(&D2_method);
}

TEST(fdm_apply_test, D2F_from_FDMapply_DF_from_FDMapply_F_xysinz)
{
  typedef struct vec_fn_params_s {
    vec_function *function;
    fdm *method;
    double *v;
  } vec_fn_params;

  // Define the finite difference approximation to DF(.)(v): U ⊂ X →
  // Y, x → DF(x)(v), where v ∈ X is given.
  auto DF_v_vec_fn = [](unsigned xdim, const double * const x, void *params,
			unsigned fdim, double * const value)->void
    {
      vec_fn_params *p = (vec_fn_params *) params;

      // Calculate the nodes {x+jhv}, 0≤j<D1_nnodes.
      fdm_calc_nodes(p->method, x, p->v);

      // Compute ∑w_jF(x+jhv)/h ≈ lim h → 0 (F(x+hv) - F(x))/h = DF(x)(v).
      fdm_apply(p->method, p->function, value);
    };

  // F(.): U ⊂ X → Y
  vec_function F;
  F.function = xysinz_vec_fn;
  F.xdim = 3;
  F.fdim = 1;
  F.params = NULL;

  double Fx;
  F.value = &Fx;

  unsigned D1_nnodes = 2;
  fdm D1_method;
  fdm_type D1_method_type = fwd;
  unsigned deriv_order = 1;
  fdm_alloc(&D1_method, D1_nnodes, F.xdim);
  fdm_set_type(&D1_method, deriv_order, D1_method_type);
  fdm_set_weights(&D1_method);

  // DF(.)(v): U ⊂ X → Y
  vec_function DF_v;
  DF_v.function = DF_v_vec_fn;

  // Both F(.) and DF(.)(v) are functions from U ⊂ X to Y (v ∈ X is
  // fixed).
  DF_v.xdim = F.xdim;
  DF_v.fdim = F.fdim;

  double DFx_v;
  DF_v.value = &DFx_v;

  fdm D2_method;
  unsigned D2_nnodes = 2;
  fdm_type D2_method_type = fwd;

  fdm_alloc(&D2_method, D2_nnodes, DF_v.xdim);
  fdm_set_type(&D2_method, deriv_order, D2_method_type);
  fdm_set_weights(&D2_method);

  double x[] = {2.0, -2.0, M_PI/6};
  double v[] = {1.0, 2.0, 1.0};

  vec_fn_params DF_v_params;
  DF_v.params = &DF_v_params;
  DF_v_params.function = &F;
  DF_v_params.method = &D1_method;
  DF_v_params.v = v;

  // Given u ∈ X and x ∈ U ⊂ X, calculate the nodes {x_j}, 0≤j<D2_nnodes.
  double u[] = {1.0, 1.0, -1.0};
  fdm_calc_nodes(&D2_method, x, u);

  // D²F(x)(.,.) is a bounded bilinear mapping from X×X to Y. Since
  // here we have Y = Rᶠᵈⁱᵐ = R¹, one double is precisely the storage
  // needed for D2Fx_uv.
  double D2Fx_uv;

  // Given u, v ∈ X, and x ∈ U ⊂ X, compute the finite difference
  // approximation to D²F(x)(u,v) using ∑w_jDF(x_j)(v)/h.
  fdm_apply(&D2_method, &DF_v, &D2Fx_uv);

  double relerr = 1e-4;
  double D2Fx_uv_expected = -0.5 - M_SQRT3;
  double abserr = relerr * fabs(D2Fx_uv_expected);

  EXPECT_NEAR(D2Fx_uv, D2Fx_uv_expected, abserr);

  fdm_free(&D1_method);
  fdm_free(&D2_method);
}
