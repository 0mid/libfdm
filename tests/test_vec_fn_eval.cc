// test_vec_fn_eval.cc ---

// Copyright (C) 2016 Omid Khanmohamadi <>

// Author: Omid Khanmohamadi <>

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "src/vec_function.h" // vec_function, VEC_FN_EVAL
#include "tst_functions.h"    // qform1d, xysinz, etc.

#include "gtest/gtest.h"    // declarations for Google Test framework.

TEST(VEC_FN_EVAL_test, qform_1d)
{
  vec_function F;
  F.function = qform1d_vec_fn;
  F.xdim = 1;
  F.fdim = 1;

  // An implicit conversion from any pointer method_type to void * is
  // allowed, because void * is a defined to be a pointer method_type that
  // has a sufficient range that it can represent any value that any
  // other pointer method_type can. (Technically, only other object pointer
  // types, which excludes pointers to functions)
  // [https://stackoverflow.com/a/2626947].
  double a = 0.5;
  F.params = &a;

  double Fx;
  F.value = &Fx;

  double x = 1.0;
  VEC_FN_EVAL(&F, &x);

  double Fvalue_expected = a*x*x;

  EXPECT_DOUBLE_EQ(Fvalue_expected, F.value[0]);
}


TEST(VEC_FN_EVAL_test, xysinz)
{

  vec_function F;
  F.function = xysinz_vec_fn;
  F.xdim = 3;
  F.fdim = 1;
  F.params = NULL;

  double Fx;
  F.value = &Fx;

  double x[] = {1, 1, M_PI_2};
  VEC_FN_EVAL(&F, x);

  double Fvalue_expected = 1.0;

  EXPECT_DOUBLE_EQ(Fvalue_expected, F.value[0]);
}
