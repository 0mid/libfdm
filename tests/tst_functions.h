// tst_functions.h ---

// Copyright (C) 2017 Omid Khanmohamadi <>

// Author: Omid Khanmohamadi <>

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#ifndef TST_FUNCTIONS_H_INCLUDED
#define TST_FUNCTIONS_H_INCLUDED

#ifndef M_PI
#define M_PI       3.14159265358979323846264338328 // pi
#endif

#ifndef M_SQRT3
#define M_SQRT3    1.73205080756887729352744634151 // sqrt(3)
#endif

double qform1d(double x, double a);

void qform1d_vec_fn(unsigned xdim, const double *x, void *params,
		    unsigned fdim, double *value);

double xysinz(unsigned xdim, const double *x);

void xysinz_vec_fn(unsigned xdim, const double * const x,
		   void *params,
		   unsigned fdim, double * const value);

void fR2_R3_vec_fn(unsigned xdim, const double * const x, void *params,
		   unsigned fdim, double * const value);

#endif	// TST_FUNCTIONS_H_INCLUDED
