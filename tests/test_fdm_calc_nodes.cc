// test_fdm_calc_nodes.cc ---

// Copyright (C) 2016 Omid Khanmohamadi <>

// Author: Omid Khanmohamadi <>

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "src/finite_diff_method.h"

#include "gtest/gtest.h"	// for Google Test framework

TEST(fdm_calc_nodes_test, D1_fwd_cen_bak_2node_1d)
{
  unsigned	xdim	    = 1;
  unsigned	nnodes	    = 2;
  fdm		method;
  fdm_type	method_type = fwd;
  unsigned	deriv_order = 1;

  fdm_alloc(&method, nnodes, xdim);
  fdm_set_type(&method, deriv_order, method_type);

  double x = 1.0; // x ∈ R: point at which to eval. lin. approx. (derivative)
  double v = 2.0; // v ∈ R: perturbation (direction) vector

  fdm_calc_nodes(&method, &x, &v);
  double *nodes = method.nodes; // nnodes*xdim elements (2*1)

  const double h = 1e-6;
  double fwd_nodes_expected[] = {x, x + h*v};

  for (unsigned i = 0; i < nnodes*xdim; ++i){
    EXPECT_DOUBLE_EQ(fwd_nodes_expected[i], nodes[i]);
  }

  method_type = cen;
  fdm_set_type(&method, deriv_order, method_type);
  fdm_calc_nodes(&method, &x, &v);

  double cen_nodes_expected[] = {x - h*v, x + h*v};

  for (unsigned i = 0; i < nnodes*xdim; ++i){
    EXPECT_DOUBLE_EQ(cen_nodes_expected[i], nodes[i]);
  }

  method_type = bak;
  fdm_set_type(&method, deriv_order, method_type);
  fdm_calc_nodes(&method, &x, &v);

  double bak_nodes_expected[] = {x - h*v, x};

  for (unsigned i = 0; i < nnodes*xdim; ++i){
    EXPECT_DOUBLE_EQ(bak_nodes_expected[i], nodes[i]);
  }

  fdm_free(&method);
}

TEST(fdm_calc_nodes_test, D1_fwd_cen_bak_3d)
{
  unsigned	xdim	    = 3;
  unsigned	nnodes	    = 4;
  fdm		method;
  fdm_type	method_type = fwd;
  unsigned	deriv_order = 1;

  fdm_alloc(&method, nnodes, xdim);
  fdm_set_type(&method, deriv_order, method_type);

  double x[] = {1.0, -1.0,  0.0};
  double v[] = {2.0,  3.0, -1.0};

  fdm_calc_nodes(&method, x, v);
  double *nodes = method.nodes;

  const double h = 1e-6;
  double fwd_nodes_expected[] = {
    x[0],            x[1],            x[2],
    x[0] +   h*v[0], x[1] +   h*v[1], x[2] +   h*v[2],
    x[0] + 2*h*v[0], x[1] + 2*h*v[1], x[2] + 2*h*v[2],
    x[0] + 3*h*v[0], x[1] + 3*h*v[1], x[2] + 3*h*v[2]
  };

  for (unsigned i = 0; i < nnodes*xdim; ++i){
    EXPECT_DOUBLE_EQ(fwd_nodes_expected[i], nodes[i]);
  }

  method_type = cen;
  fdm_set_type(&method, deriv_order, method_type);
  fdm_calc_nodes(&method, x, v);

  double cen_nodes_expected[] = {
    x[0] - 2*h*v[0], x[1] - 2*h*v[1], x[2] - 2*h*v[2],
    x[0] -   h*v[0], x[1] -   h*v[1], x[2] -   h*v[2],
    x[0] +   h*v[0], x[1] +   h*v[1], x[2] +   h*v[2],
    x[0] + 2*h*v[0], x[1] + 2*h*v[1], x[2] + 2*h*v[2]
  };

  for (unsigned i = 0; i < nnodes*xdim; ++i){
    EXPECT_DOUBLE_EQ(cen_nodes_expected[i], nodes[i]);
  }

  method_type = bak;
  fdm_set_type(&method, deriv_order, method_type);
  fdm_set_weights(&method);

  fdm_calc_nodes(&method, x, v);

  double bak_nodes_expected[] = {
    x[0] - 3*h*v[0], x[1] - 3*h*v[1], x[2] - 3*h*v[2],
    x[0] - 2*h*v[0], x[1] - 2*h*v[1], x[2] - 2*h*v[2],
    x[0] -   h*v[0], x[1] -   h*v[1], x[2] -   h*v[2],
    x[0],            x[1],            x[2]
  };

  for (unsigned i = 0; i < nnodes*xdim; ++i){
    EXPECT_DOUBLE_EQ(bak_nodes_expected[i], nodes[i]);
  }

  fdm_free(&method);
}
