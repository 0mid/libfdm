// test_fdm_form_Dfunc_pt_matrix.cc ---

// Copyright (C) 2016 Omid Khanmohamadi <>

// Author: Omid Khanmohamadi <>

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "src/finite_diff_method.h"
#include "tst_functions.h"

#include "gtest/gtest.h"	// for Google Test framework

#include <stdlib.h>		// for malloc(), free(), NULL

TEST(fdm_form_Dfunc_pt_matrix, D1_cen_4node_xysinz)
{
  vec_function F;

  F.function = xysinz_vec_fn;
  F.xdim     = 3;
  F.fdim     = 1;
  F.params   = NULL;

  double F_x; F.value = &F_x;

  unsigned	nnodes	    = 2;
  fdm		method;
  fdm_type	method_type = cen;
  unsigned	deriv_order = 1;

  fdm_alloc(&method, nnodes, F.xdim);
  fdm_set_type(&method, deriv_order, method_type);
  fdm_set_weights(&method);

  Dfunc_pt_workspace DF_x_workspc;
  Dfunc_pt_workspace_alloc(&DF_x_workspc, F.xdim, F.fdim);
  double * const DF_x_matrix = DF_x_workspc.matrix;

  double x[] = {2.0, 3.0, M_PI_2};
  fdm_form_Dfunc_pt_matrix(&method, &F, x, &DF_x_workspc);

  double relerr = 1e-5;
  double DF_x_matrix_expected[] = {3.0, 2.0, 0.0};

  for (unsigned i = 0; i < F.fdim*F.xdim; ++i) {
    double abserr = relerr * fabs(DF_x_matrix_expected[i]);

    EXPECT_NEAR(DF_x_matrix[i],
		DF_x_matrix_expected[i], abserr);
  }

  fdm_free(&method);
  Dfunc_pt_workspace_free(&DF_x_workspc);
}


TEST(fdm_form_Dfunc_pt_matrix, D1_cen_4node_fR2_R3)
{
  const unsigned xdim = 2;
  const unsigned fdim = 3;

  vec_function F;

  F.function = fR2_R3_vec_fn;
  F.xdim     = xdim;
  F.fdim     = fdim;
  F.params   = NULL;

  double F_x[fdim]; F.value = F_x;

  unsigned	nnodes	    = 4;
  fdm		method;
  fdm_type	method_type = cen;
  unsigned	deriv_order = 1;

  fdm_alloc(&method, nnodes, xdim);
  fdm_set_type(&method, deriv_order, method_type);
  fdm_set_weights(&method);

  Dfunc_pt_workspace DF_x_workspc;
  Dfunc_pt_workspace_alloc(&DF_x_workspc, xdim, fdim);
  double * const DF_x_matrix = DF_x_workspc.matrix;

  double x[] = {-1.0, 1.0};
  fdm_form_Dfunc_pt_matrix(&method, &F, x, &DF_x_workspc);

  double relerr = 1e-5;
  double DF_x_matrix_expected[] = {1.0, 1.0, -2.0,   // column 0
				  -1.0, 1.0, -2.0};  // column 1

  for (unsigned i = 0; i < F.fdim*F.xdim; ++i) {
    double abserr = relerr * fabs(DF_x_matrix_expected[i]);

    EXPECT_NEAR(DF_x_matrix[i],
		DF_x_matrix_expected[i], abserr);
  }

  fdm_free(&method);
  Dfunc_pt_workspace_free(&DF_x_workspc);
}
