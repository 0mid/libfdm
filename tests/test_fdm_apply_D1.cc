// test_fdm_apply_D1.cc ---

// Copyright (C) 2016 Omid Khanmohamadi <>

// Author: Omid Khanmohamadi <>

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "src/finite_diff_method.h"
#include "tst_functions.h"

#include "gtest/gtest.h"	// for Google Test framework

#include <stdlib.h>		// for malloc(), free(), NULL

TEST(fdm_apply_test, D1_fwd_cen_2node_qform1d)
{
  vec_function F;

  F.function = qform1d_vec_fn;
  F.xdim     = 1;
  F.fdim     = 1;

  double a = 0.5;
  F.params = &a;

  double qform1d_value;
  F.value = &qform1d_value;

  unsigned	nnodes	    = 2;
  fdm		method;
  fdm_type	method_type = fwd;
  unsigned	deriv_order = 1;

  fdm_alloc(&method, nnodes, F.xdim);
  fdm_set_type(&method, deriv_order, method_type);
  fdm_set_weights(&method);

  double x = 1.0; // x ∈ R: point at which to form lin. approx. (derivative)
  double v = 2.0; // v ∈ R: perturbation (direction) vector

  fdm_calc_nodes(&method, &x, &v);

  double* DFxv = (double *) malloc(F.fdim * sizeof(double));
  fdm_apply(&method, &F, DFxv);

  double	relerr	      = 1e-5;
  double	DFxv_expected = 2*a*x*v;
  double	abserr	      = relerr * DFxv_expected;

  EXPECT_NEAR(DFxv[0], DFxv_expected, abserr);

  method_type = cen;
  fdm_set_type(&method, deriv_order, method_type);
  fdm_set_weights(&method);
  fdm_calc_nodes(&method, &x, &v);
  fdm_apply(&method, &F, DFxv);

  EXPECT_NEAR(DFxv[0], DFxv_expected, abserr);

  fdm_free(&method);
  free(DFxv);
}

TEST(fdm_apply_test, D1_fwd_cen_6node_xysinz)
{
  vec_function F;
  F.function = xysinz_vec_fn;
  F.xdim = 3;
  F.fdim = 1;

  F.params = NULL;

  unsigned	nnodes	    = 6;
  fdm		method;
  fdm_type	method_type = fwd;
  unsigned	deriv_order = 1;

  fdm_alloc(&method, nnodes, F.xdim);
  fdm_set_type(&method, deriv_order, method_type);
  fdm_set_weights(&method);

  double x[] = {1.0, 1.0, M_PI_2};
  double v[] = {1.0, 2.0, 1.0};
  fdm_calc_nodes(&method, x, v);

  double DFxv[] = {0.0};
  double Fx; F.value = &Fx;
  fdm_apply(&method, &F, DFxv);

  double relerr = 1e-5;
  double DFxv_expected = 3.0;
  double abserr = relerr * DFxv_expected;

  EXPECT_NEAR(DFxv[0], DFxv_expected, abserr);

  method_type = cen;
  fdm_set_type(&method, deriv_order, method_type);
  fdm_set_weights(&method);
  fdm_calc_nodes(&method, x, v);
  fdm_apply(&method, &F, DFxv);

  EXPECT_NEAR(DFxv[0], DFxv_expected, abserr);

  fdm_free(&method);
}

TEST(fdm_apply_test, D1_fwd_3node_fR2_R3)
{
  vec_function F;
  F.function = fR2_R3_vec_fn;
  const unsigned xdim = 2, fdim = 3;
  F.xdim = xdim;
  F.fdim = fdim;

  F.params = NULL;

  unsigned nnodes = 3;
  fdm method;
  fdm_type method_type = fwd;
  unsigned deriv_order = 1;
  fdm_alloc(&method, nnodes, xdim);
  fdm_set_type(&method, deriv_order, method_type);
  fdm_set_weights(&method);

  double x[] = {M_PI_2, M_PI_2}; // {π/2, π/2}
  double v[] = {1.0, 1.0};
  fdm_calc_nodes(&method, x, v);

  double Fx[fdim]; F.value = Fx;
  double DFxv[fdim];
  fdm_apply(&method, &F, DFxv);

  double relerr = 1e-5;
  double DFxv_expected[] = {M_PI, -2.0, 0.0}; // {π, -2, 0}

  for (unsigned i = 0; i < fdim; ++i){
    double abserr_i = relerr * fabs(DFxv_expected[i]);
    EXPECT_NEAR(DFxv[i], DFxv_expected[i], abserr_i);
  }

  fdm_free(&method);
}
