// test_fdm_set_weights.cc ---

// Copyright (C) 2016 Omid Khanmohamadi <>

// Author: Omid Khanmohamadi <>

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "gtest/gtest.h"	// for Google Test framework

#include "src/finite_diff_method.h"
#include "src/array_funcro.h"

TEST(fdm_set_weights_test, cen_4node)
{
  unsigned	xdim	    = 3;
  unsigned	nnodes	    = 4;
  fdm		method;
  fdm_type	method_type = cen;
  unsigned	deriv_order = 1;

  fdm_alloc(&method, nnodes, xdim);
  fdm_set_type(&method, deriv_order, method_type);
  fdm_set_weights(&method);

  double wf_expected[] = {0.66666666666666666666, -0.08333333333333333333};
  const double * const wf = method.table->wf;

  for (unsigned i = 0; i < NELEMS(wf_expected); ++i){
    EXPECT_DOUBLE_EQ(wf_expected[i], wf[i]);
  }

  fdm_free(&method);
}
