# This is a convenience library, used only when building the package.
# We link against this library to build our main and test executable
# files.
#
# https://stackoverflow.com/a/2305737
noinst_LTLIBRARIES = libfdm.la


# The sources of each target go into _SOURCES, with non-alphanumeric
# characters in the name of the libraries mapped to '_' (e.g., .la
# becomes _la).
#
# Automake automatically computes the list of objects to build and
# link from these files. Header files are not compiled. We list them
# only so they get distributed (Automake does not distribute files it
# does not know about), and, of course, to make sure their target is
# updated if they change. Unless otherwise specified, compiler and
# linker are inferred from the extensions.
#
# Usually, only header files that accompany installed libraries need
# to be installed. Headers used by programs or convenience libraries
# are not installed. The noinst_HEADERS variable can be used for such
# headers. However when the header actually belongs to a single
# convenience library or program, we recommend listing it in the
# program's or library's _SOURCES variable instead of in
# noinst_HEADERS. This is clearer for the Makefile.am reader.
# noinst_HEADERS would be the right variable to use in a directory
# containing only headers and no associated library or program.
# [https://www.gnu.org/software/automake/manual/html_node/Headers.html]

libfdm_la_SOURCES =			\
 return_code.h				\
 vec_function.h				\
 finite_diff_method.h			\
 finite_diff_table.h			\
 finite_diff_method.cc

# Non-default flags should NOT be added to Autoconf files; doing so
# violates the principal of least surprise, as anyone familiar with
# Autoconf expects the default flags to be '-g -O2' for CXXFLAGS and
# you should not change that. If a user wants anything but the
# default, they can configure/make the package with their non-default
# options, e.g., with
#
# ./configure CXXFLAGS='-g -ggdb -gdwarf-3 -O0'
#
# to get a version suitable for debugging with DWARF-3 compatible GDB's, or
#
# make check CXXFLAGS='-O3'
#
# to get a version with extra optimizations.
# [https://stackoverflow.com/a/3147058, https://stackoverflow.com/a/4680578].
libinquad_la_CXXFLAGS = -Wall -Wextra -std=c++11
