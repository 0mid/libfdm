// array_funcro.h ---

// Copyright (C) 2016 Omid Khanmohamadi <>

// Author: Omid Khanmohamadi <>

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#ifndef ARRAY_FUNCRO_H_INCLUDED
#define ARRAY_FUNCRO_H_INCLUDED

// When sent as an argument to a function, an array A "decays" to a
// pointer. As a result, the following macro expands to 1 if used on
// A, regardless of the actual size of A.
#define NELEMS(A) (sizeof(A)/sizeof((A)[0]))

#endif	// ARRAY_FUNCRO_H_INCLUDED
