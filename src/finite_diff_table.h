// finite_diff_table.h ---

// Copyright (C) 2016 Omid Khanmohamadi <>

// Author: Omid Khanmohamadi <>

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#ifndef FINITE_DIFF_TABLE_H_INCLUDED
#define FINITE_DIFF_TABLE_H_INCLUDED

#include "array_funcro.h"

// The idea of table merging and look-up is based on that of GSL in
// 'integration/glfixed.c'.

// In C++, a const variable has internal linkage by default, so static
// const is redundant and extern const is necessary to get a constant
// with external linkage. Explicitly using static with const in C++,
// however, ensures that the behavior is the same in both C and C++.

static const double D1_fwd_wf2[] = {
  -1.0,
   1.0
};

static const double D1_fwd_wf3[] = {
  -1.5,
   2.0,
  -0.5
};

static const double D1_fwd_wf4[] = {
  -1.83333333333333333333,
   3.0,
  -1.5,
   0.33333333333333333333
};

static const double D1_fwd_wf5[] = {
  -2.08333333333333333333,
   4.0,
  -3.0,
   1.33333333333333333333,
  -0.25
};

static const double D1_fwd_wf6[] = {
  -2.28333333333333333333,
   5.0,
  -5.0,
   3.33333333333333333333,
  -1.25,
   0.2
};

// Order of accuracy = # points - 1
static fdm_tbl D1_fwd_diff_tbl[] = {
  { 2, D1_fwd_wf2 },
  { 3, D1_fwd_wf3 },
  { 4, D1_fwd_wf4 },
  { 5, D1_fwd_wf5 },
  { 6, D1_fwd_wf6 }
};


static const double D2_fwd_wf3[] = {
  1.0,
 -2.0,
  1.0
};

static const double D2_fwd_wf4[] = {
  2.0,
 -5.0,
  4.0,
 -1.0
};

static const double D2_fwd_wf5[] = {
  2.91666666666666666666,
 -8.66666666666666666666,
  9.5,
 -4.66666666666666666666,
  0.91666666666666666666
};

static const double D2_fwd_wf6[] = {
   3.75,
 -12.83333333333333333333,
  17.83333333333333333333,
 -13.0,
   5.08333333333333333333,
  -0.83333333333333333333
};

static const double D2_fwd_wf7[] = {
    4.51111111111111111111,
   -17.4,
    29.24,
   -28.22222222222222222222,
    16.5,
    -5.4,
     0.76111111111111111111
};

// Order of accuracy = # points - 2
static fdm_tbl D2_fwd_diff_tbl[] = {
    { 3, D2_fwd_wf3 },
    { 4, D2_fwd_wf4 },
    { 5, D2_fwd_wf5 },
    { 6, D2_fwd_wf6 }
};

static fdm_tbl *fwd_diff_tbl[] = {
  D1_fwd_diff_tbl,
  D2_fwd_diff_tbl
};

static const unsigned fwd_diff_tbl_sizes[] = {
  NELEMS(D1_fwd_diff_tbl),
  NELEMS(D2_fwd_diff_tbl)
};



static const double D1_cen_wf2[] = {
  // -0.5,
   0.5
};

static const double D1_cen_wf4[] = {
  //  0.08333333333333333333,
  // -0.66666666666666666666,
   0.66666666666666666666,
  -0.08333333333333333333
};

static const double D1_cen_wf6[] = {
  // -0.01666666666666666666,
  //  0.15,
  // -0.75,
   0.75,
  -0.15,
   0.01666666666666666666
};

static const double D1_cen_wf8[] = {
  //  0.00357142857142857142,
  // -0.03809523809523809523,
  //  0.2,
  // -0.8,
   0.8,
  -0.2,
   0.03809523809523809523,
  -0.00357142857142857142
};

// Order of accuracy = # points
static fdm_tbl D1_cen_diff_tbl[] = {
    { 2, D1_cen_wf2 },
    { 4, D1_cen_wf4 },
    { 6, D1_cen_wf6 },
    { 8, D1_cen_wf8 }
};

static const double D2_cen_wf3[] = {
  // 1.0,
 -2.0,
  1.0
};

static const double D2_cen_wf5[] = {
  // -0.08333333333333333333,
  //  1.33333333333333333333,
  -2.5,
   1.33333333333333333333,
  -0.08333333333333333333
};

static const double D2_cen_wf7[] = {
 //  0.01111111111111111111,
 // -0.15,
 //  1.5,
 -2.72222222222222222222,
  1.5,
 -0.15,
  0.01111111111111111111
};

// Order of accuracy = # points
static fdm_tbl D2_cen_diff_tbl[] = {
    { 3, D2_cen_wf3 },
    { 5, D2_cen_wf5 },
    { 7, D2_cen_wf7 }
};

static fdm_tbl *cen_diff_tbl[] = {
  D1_cen_diff_tbl,
  D2_cen_diff_tbl
};

static const unsigned cen_diff_tbl_sizes[] = {
  NELEMS(D1_cen_diff_tbl),
  NELEMS(D2_cen_diff_tbl)
};

#endif // FINITE_DIFF_TABLE_H_INCLUDED
