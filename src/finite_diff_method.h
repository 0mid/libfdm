// finite_diff_method.h ---

// Copyright (C) 2016 Omid Khanmohamadi <>

// Author: Omid Khanmohamadi <>

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#ifndef FINITE_DIFF_METHOD_H_INCLUDED
#define FINITE_DIFF_METHOD_H_INCLUDED

#include "vec_function.h"
#include "return_code.h"

typedef enum fdm_type_e {
  fwd,
  bak,
  cen,
  stag
} fdm_type;

// Members in this struct CANNOT be reordered without reordering the
// _tbl assignments in finite_diff_table.h.
typedef struct fdm_tbl_s {
  unsigned n;		   // number of nodes
  const double *wf;	   // n weights for f, in wf[i]*f(x + i*h*v)
} fdm_tbl;


typedef struct fdm_s {
  fdm_tbl *table;
  double *nodes; // xdim × n storage for x+i*h, x ∈ Rˣᵈⁱᵐ, 0 ≤ i < n
  unsigned n;
  unsigned xdim;
  fdm_type type;
  unsigned deriv_order;
} fdm;

void fdm_alloc(fdm * const method,
	       unsigned n,
	       unsigned xdim);


void fdm_set_type(fdm * const method,
		  unsigned deriv_order,
		  fdm_type method_type);

void fdm_set_weights(fdm * const method);

void
fdm_calc_nodes(fdm * const method,
	       const double * const x,
	       const double * const v);

void fdm_free(fdm * const method);

void
fdm_apply(fdm * const method,
	  const vec_function * const F,
	  double * const result);

// Workspace for the matrix representation of the derivative of a
// function at a point.
typedef struct Dfunc_pt_workspace_s {
  double *vector;		// len: xdim
  double *matrix;		// len: fdim × xdim
} Dfunc_pt_workspace;

errcode
Dfunc_pt_workspace_alloc(Dfunc_pt_workspace * DF_x_workspc,
			 unsigned xdim,
			 unsigned fdim);


void Dfunc_pt_workspace_free(Dfunc_pt_workspace * const DF_x_workspc);

void
fdm_form_Dfunc_pt_matrix(fdm * const method,
			 const vec_function * const F,
			 const double * const x,
			 Dfunc_pt_workspace * const DF_x_workspc);

#endif	// FINITE_DIFF_METHOD_H_INCLUDED
