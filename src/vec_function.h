// vec_function.h ---

// Copyright (C) 2017 Omid Khanmohamadi <>

// Author: Omid Khanmohamadi <>

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#ifndef VEC_FUNCTION_H_INCLUDED
#define VEC_FUNCTION_H_INCLUDED

typedef struct vec_function_s {
  void (* function) (unsigned xdim, const double * const x, void *params,
		     unsigned fdim, double * const value);

  double	*value;
  void		*params;
  unsigned	 xdim;
  unsigned	 fdim;
} vec_function;

#define VEC_FN_EVAL(F,x) (*((F)->function))((F)->xdim, x, (F)->params,	\
					    (F)->fdim, (F)->value)

#endif	// VEC_FUNCTION_H_INCLUDED
