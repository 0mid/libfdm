// finite_diff_method.cc ---

// Copyright (C) 2016 Omid Khanmohamadi <>

// Author: Omid Khanmohamadi <>

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "finite_diff_method.h"
#include "finite_diff_table.h"

#include <stdlib.h>		// for malloc, free, NULL

static const double h = 1e-6;	// TODO: Find optimal h
static const double h_recip = 1.0/h;

// Look-up
fdm_tbl *fdm_tbl_get(unsigned deriv_order,
				     fdm_type method_type,
				     unsigned n)
{
  fdm_tbl *tbl;
  unsigned tbl_size;

  unsigned tbl_row = deriv_order - 1; // row # starts at 0, deriv_order at 1.

  switch (method_type){
  case fwd: default:
    tbl	     = fwd_diff_tbl[tbl_row];
    tbl_size = fwd_diff_tbl_sizes[tbl_row];

    break;

  case cen:
    tbl	     = cen_diff_tbl[tbl_row];
    tbl_size = cen_diff_tbl_sizes[tbl_row];

    break;
  }

  for (unsigned i = 0; i < tbl_size; ++i){
    if (n == tbl[i].n){
      return &tbl[i];
    }
  }
}

void fdm_alloc(fdm * const method,
	       unsigned n,
	       unsigned xdim)
{
  // TODO: Check for malloc's success.
  method->nodes = (double *) malloc(n*xdim * sizeof(double));

  method->n	= n;
  method->xdim	= xdim;
}

void fdm_set_type(fdm * const method,
		  unsigned deriv_order,
		  fdm_type method_type)
{
  method->type = method_type;
  method->deriv_order = deriv_order;
}

void fdm_set_weights(fdm * const method)
{
  unsigned	n	    = method->n;
  fdm_type	method_type = method->type;
  unsigned	deriv_order = method->deriv_order;

  method->table = fdm_tbl_get(deriv_order, method_type, n);
}

// We may have several functions (e.g., level-set functions) all using
// the same finite difference method, in which case they will all be
// using the same nodes {x+jhv}, 0 ≤ j < n. Keeping the functions
// 'fdm_calc_nodes' and 'fdm_apply' separate allows the nodes to be
// calculated only once and then used by all functions.
void fdm_calc_nodes(fdm * const method,
		    const double * const x,
		    const double * const v)
{

  unsigned		xdim	    = method->xdim;
  unsigned		n	    = method->n;
  double * const	nodes	    = method->nodes;
  unsigned		deriv_order = method->deriv_order;
  fdm_type		method_type = method->type;

  // Since we do signed arithmetic involving the following variables,
  // they MUST be declared as 'int', rather than 'unsigned' (int).
  // Otherwise, there shall be dragons!
  int j, jmid, offset_left_js, offset_right_js;
  if (method_type == cen){
    if (deriv_order % 2 == 1){	// for Dᵒᵈᵈ (so, n is even)
      // Nodes with 0≤j<jmid are on the left, with jmid≤j<n on the right.
      jmid     = n/2;

      // Amount to shift j for the nodes on the left. Calculate xi +
      // (j+offset_left_js)*h*v_i for 0≤j<jmid.
      offset_left_js = -jmid;

      // Amount to shift j for the nodes on the right. Calculate xi +
      // (j+offset_right_js)*h*v_i for jmid≤j<n.
      offset_right_js = offset_left_js + 1;
    }

    else {			// for Dᵉᵛᵉⁿ (so, n is odd)
      jmid	      = (n-1)/2;
      offset_left_js  = -jmid;
      offset_right_js = offset_left_js;
    }
  } // if (method_type == cen)

  else if (method_type == fwd){
    jmid	    = 0;
    offset_left_js  = 0;
    offset_right_js = offset_left_js;
  }

  else if (method_type == bak){
    jmid	    = n;
    offset_left_js  = -jmid + 1;
    offset_right_js = 0;
  }

  for (unsigned i = 0; i < xdim; ++i){ // for every component of x
    double xi = x[i], h_vi = h*v[i];

    unsigned offset_nodej, ij;
    for (j = 0; j < jmid; ++j){ // for left nodes of method
      offset_nodej = j*xdim;

      // TODO: Research/Test if performance of writes w/ non-unit
      // stride is worse than that of reads. If so, reverse order of loops.
      ij = i + offset_nodej;
      nodes[ij] = xi + (j + offset_left_js) * h_vi;
    }

    for (j = jmid; j < n; ++j){ // for right nodes of method
      offset_nodej = j*xdim;

      // TODO: Ditto.
      ij = i + offset_nodej;
      nodes[ij] = xi + (j + offset_right_js) * h_vi;
    }
  }
}

// Let X and Y be normed vector spaces. Given a function F: U ⊂ X → Y
// and a point x ∈ U, the (Frechet) derivative of F at x, denoted
// DF(x)(.), (if it exists) is the best linear approximation to F at
// x; i.e., DF(x): X → Y is the unique linear mapping (in L(X,Y), the
// space of bounded linear mappings from X to Y) for which
//
// lim v → 0 ||F(x+v)-F(x) - DF(x)(v)||/||v|| = 0.
//
// We can extrapolate all directional derivatives of a differentiable
// function from its derivative:
//
// lim h → 0 ( F(x+hv)-F(x) )/h = DF(x)(v). (*)
//
// The function fdm_apply approximates the RHS of (*) by approximating
// its LHS using ∑w_jF(x+jhv)/h, where the nodes {x+jhv} and weights
// {w_j} come from the finite difference 'method' supplied.
void fdm_apply(fdm * const method,
	       const vec_function * const F,
	       double * const result)
{

  unsigned xdim = F->xdim, fdim = F->fdim;

  unsigned		n	    = method->table->n;
  const double * const	wf	    = method->table->wf;
  const double * const	nodes	    = method->nodes;
  unsigned		deriv_order = method->deriv_order;
  fdm_type		method_type = method->type;

  // Every time this 'function' is called , 'result' must be
  // initialized to zero. Otherwise, we accumulate not only the
  // current results, but also those from the last run. That's why the
  // initialization is done here and not in the method allocator.
  unsigned i;
  for (i = 0; i < fdim; ++i){
    result[i] = 0.0;
  }

  if (method_type == cen){
    unsigned endj = n - 1;	// index of last node

    if (deriv_order % 2 == 1){	// if Dᵒᵈᵈ (so, n even)
      unsigned m = n/2;	   // # of nodes to the left & right of center
      // Offset (mode xdim) of the last node (counting from the left)
      // to the left of center, pointed to by nodes + endm*xdim.
      unsigned endm = m - 1;

      // ∑{0≤j<n, n even} w_jF(x_j)/h					(A)
      // = ∑{0≤j<m} (w_jF(x_j)+w_{-j}F(x_{-j}))/h, m = n/2		(B)
      // = ∑{0≤j<m} w_j(F(x_j)-F(x_{-j}))/h, w_{-j} = -w_j for n even	(C)

      // Since F->value is overwritten every time VEC_FN_EVAL is
      // 'called' on F, implementing (C) using one inner loop (for
      // 'fdim' components of F) requires a temporary array of 'fdim'
      // doubles to which 'fdim' doubles from F->value (the value of F
      // at a left or right node) need to be COPIED. To avoid this
      // scenario, we implement (C) using two inner loops.

      unsigned offset_nodej_left  = 0;
      unsigned offset_nodej_right = endj*xdim;
      for (unsigned j = 0; j < m; ++j){

	// The weight in position j of wf array goes with the nodes in
	// position j and endj-j; its sign for the node in position j
	// is the opposite of that of the node in position endj-j.
	double wf_nodej_h_recip = wf[endm - j] * h_recip;

	VEC_FN_EVAL(F, nodes + offset_nodej_left);

	for (i = 0; i < fdim; ++i){
	  // F(nodej_left) comes in with -w_j.
	  result[i] -= F->value[i] * wf_nodej_h_recip;
	}

	// TODO: Check for cache misses, since we are using non-unit,
	// negative strides.`getconf LEVEL1_DCACHE_LINESIZE` is 64
	// bytes on x86-64.
	VEC_FN_EVAL(F, nodes + offset_nodej_right);

	for (i = 0; i < fdim; ++i){
	  // F(nodej_right) comes in with +w_j.
	  result[i] += F->value[i] * wf_nodej_h_recip;
	}

	offset_nodej_left  += xdim; // shift one node forward from the left
	offset_nodej_right -= xdim; // and one node backward from the right
      }
    } // if Dᵒᵈᵈ (so, n even)

    else {		    // if Dᵉᵛᵉⁿ (so, n odd)

      unsigned	m    = (n-1)/2; // # of nodes to the left & right of center

      // ∑{0≤j<n, n odd} w_jF(x_j)/h
      // = w_mF(x_m)+∑{0≤j<m} (w_jF(x_j)+w_{-j}F(x_{-j}))/h, m = (n-1)/2
      // = w_mF(x_m)+∑{0≤j<m} (w_jF(x_j)+w_jF(x_{-j}))/h, w_{-j} = w_j for n odd

      // w_{-m+0},        w_{-m+1},       ..., w{-1}
      // x_{-m+0},        x_{-m+1},       ..., x{-1}
      // nodes+(0)*xdim,  nodes+(1)*xdim, ...,  nodes+(m-1)*xdim
      // -wf[m-(0)],     -wf[m-(1)],      ..., -wf[m-(m-1)]
      //
      // So, wf[m-j]F(nodes+j*xdim)/h, for 0≤j<m
      //
      // Node at center:
      // x₀
      // nodes+(endj-m)*xdim == nodes+(m)*xdim
      // wf[0]
      //
      // Nodes on the right:
      // x_{1},                   ..., x_{m-1},             x_{m}
      // nodes+(endj-(m-1))*xdim, ..., nodes+(endj-1)*xdim, nodes+(endj-0)*xdim
      // wf[1],                   ..., wf[m-1],             wf[m]
      //
      // So, wf[m-j]F(nodes+(endj-j)*xdim)/h, for 0≤j<m

      unsigned offset_nodej_left  = 0;
      unsigned offset_nodej_right = endj*xdim;
      unsigned j;
      for (j = 0; j < m; ++j){

	double wf_nodej_h_recip = wf[m - j] * h_recip;

	VEC_FN_EVAL(F, nodes + offset_nodej_left);

	for (i = 0; i < fdim; ++i){
	  // F(nodej_left) comes in with +w_j.
	  result[i] += F->value[i] * wf_nodej_h_recip;
	}

	VEC_FN_EVAL(F, nodes + offset_nodej_right);

	for (i = 0; i < fdim; ++i){
	  // F(nodej_right) comes in with +w_j.
	  result[i] += F->value[i] * wf_nodej_h_recip;
	}

	offset_nodej_left  += xdim; // move one node forward from the left
	offset_nodej_right -= xdim; // and one node backward from the right
      }

      // Now that the loop above is done, j is equal to m, xdim has
      // been added to offset_nodej_left m times and (subtracted from
      // offset_nodej_right m times). As a result, both
      // nodes+offset_nodej_left and nodes+offset_nodej_left have
      // moved m nodes and point to the center node.
      double wf_nodej_h_recip = wf[m - j] * h_recip;

      VEC_FN_EVAL(F, nodes + offset_nodej_left);

      for (i = 0; i < fdim; ++i){
	result[i] += F->value[i] * wf_nodej_h_recip;
      }

    } // if Dᵉᵛᵉⁿ (so, n odd)
  } // if (method_type == cen)

  else if (method_type == fwd){
    for (unsigned j = 0; j < n; ++j){ // for each node
      unsigned	offset_nodej = j*xdim;
      double	wf_nodej     = wf[j];

      VEC_FN_EVAL(F, nodes + offset_nodej); // evaluate F at node

      for (i = 0; i < fdim; ++i){
	// Add the finite difference contribution of each component of
	// the value of F to the corresponding component of result.
	result[i] += (wf_nodej * F->value[i]) * h_recip;
      }
    }
  }
}

void fdm_free(fdm * const method)
{
  free(method->nodes);
}


// This function may be used with, but is not limited to, a finite
// difference method. Hence, it is not prefixed by fdm.
errcode
Dfunc_pt_workspace_alloc(Dfunc_pt_workspace *DF_x_workspc,
			 unsigned xdim,
			 unsigned fdim)
{
  DF_x_workspc->vector =
    (double *) malloc( (xdim + fdim*xdim) * sizeof(double));

  if (NULL == DF_x_workspc->vector) {
    return ERR_MALLOC;
  }

  DF_x_workspc->matrix = DF_x_workspc->vector + xdim;

  for (unsigned i = 0; i < xdim; ++i) {
    DF_x_workspc->vector[i] = 0.0;
  }

  return SUCCESS;
}

void Dfunc_pt_workspace_free(Dfunc_pt_workspace * const DF_x_workspc)
{
  free(DF_x_workspc->vector);
}

// Output in DF_x_workspc->matrix, in column-wise order.
//
// Prepare DF_x_workspc by running Dfunc_pt_workspace_alloc beforehand.
//
// Given a function F: U ⊂ X → Y and a finite difference method (whose
// ->weights have been set and for whose ->nodes storage has been
// allocated), fdm_form_Dfunc_pt_matrix forms (in DFx) the
// Cartesian-coordinate matrix representation of DF(x)(.): X → Y.
//
// For example, when F is a scalar field (i.e., F: R^{xdim} → R), the
// result DF_x will be an 1 × xdim array with
//
// DF_x[j] := DF(x)(e_j) =: <∇F(x), e_j>,
//
// assuming that X is endowed with an inner product so that the RHS,
// which is /the definition/ of the gradient, makes sense, where e_j
// is that basis vector of the standard Cartesian basis for R^{xdim}
// which is 1 in position j and 0 elsewhere.
//
// When F is a vector field (i.e., F: R^{xdim} → R^{fdim}), the result
// DF_x will be an fdim × xdim array, with
//
// DF_x[fdim*j:fdim*j+fdim] := DF(x)(e_j),
//
// which corresponds to the column j of the so-called Jacobian matrix.
void fdm_form_Dfunc_pt_matrix(fdm * const method,
			      const vec_function * const F,
			      const double * const x,
			      Dfunc_pt_workspace * const DF_x_workspc)
{
  unsigned xdim = F->xdim;
  unsigned fdim = F->fdim;

  double * const	ej	   = DF_x_workspc->vector;
  double *		DF_x_col_j = DF_x_workspc->matrix;

  for (unsigned j = 0; j < xdim; ++j) {
    ej[j] = 1.0;

    fdm_calc_nodes(method, x, ej);    // output in method->nodes
    fdm_apply(method, F, DF_x_col_j); // output in DF_x_col_j

    DF_x_col_j += fdim;		// go to next column of DF_x matrix
    ej[j] = 0.0;
  }
}
